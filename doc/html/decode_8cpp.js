var decode_8cpp =
[
    [ "DecodeMethod", "decode_8cpp.html#a399a4628c6b0658774db4119adb0e74f", [
      [ "FROM_BINARY_TO_UTF8", "decode_8cpp.html#a399a4628c6b0658774db4119adb0e74fae678e6fd85ad21fe9e12536ec7dbbb82", null ],
      [ "FROM_ENGLISH_TO_RUSSIAN_LAYOUT", "decode_8cpp.html#a399a4628c6b0658774db4119adb0e74fa13d1e03ebeba9c48c7eff27acc97a9fa", null ],
      [ "GET_STICKER_ID", "decode_8cpp.html#a399a4628c6b0658774db4119adb0e74fadbda8db46abf62de7a587f0ff0ce1756", null ]
    ] ],
    [ "defineDecodeMethod", "decode_8cpp.html#af9b4037184d50313d1fb9e6b9b3ed6a1", null ],
    [ "fromBinaryToUtf8", "decode_8cpp.html#aba88a6f6fefbf041bcf6dffe06a224ed", null ],
    [ "fromEnglishToRussianLayout", "decode_8cpp.html#ae64d5e0d874bc23207daa513761a56ff", null ],
    [ "isUtf8", "decode_8cpp.html#a3020f9e3cd0f1c86119dc48e257d3bb1", null ]
];