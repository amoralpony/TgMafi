var classExecuteConditions =
[
    [ "ExecuteConditions", "classExecuteConditions.html#afeb09cb1481ba661fd098992b1b06eea", null ],
    [ "hasFocusWithVariant", "classExecuteConditions.html#ae2ee582173924371d7c86415c452d62a", null ],
    [ "isIIsAdmin", "classExecuteConditions.html#a98443966b540f260545e448b1785af80", null ],
    [ "isMessageIsReply", "classExecuteConditions.html#ae519ac2ba6e6007bcd58a7b4749a2c5c", null ],
    [ "isUserIsAdmin", "classExecuteConditions.html#af7aafebcd36072299339bdb7185e7a3b", null ],
    [ "isUserIsMe", "classExecuteConditions.html#a75abea02b17afbb26519171bf449f2e1", null ],
    [ "isUserIsTrusted", "classExecuteConditions.html#a5310b0584a4dde67a48959c3fd62ffc0", null ],
    [ "botInfo_", "classExecuteConditions.html#a20e00792864416e817954b3eceb86957", null ],
    [ "lastMessage_", "classExecuteConditions.html#a6dcd3f480e5b4a2322ef89c7c602c6e1", null ],
    [ "settings", "classExecuteConditions.html#a73cabe7d7934213ae42a5a173f2e85fe", null ]
];