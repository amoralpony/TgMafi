var annotated_dup =
[
    [ "actions", "namespaceactions.html", [
      [ "Action", "classactions_1_1Action.html", "classactions_1_1Action" ]
    ] ],
    [ "BotInfo", "structBotInfo.html", "structBotInfo" ],
    [ "ConfigManager", "classConfigManager.html", "classConfigManager" ],
    [ "ConfigValues", "structConfigValues.html", "structConfigValues" ],
    [ "ExecuteConditions", "classExecuteConditions.html", "classExecuteConditions" ],
    [ "FocusIn", "structFocusIn.html", "structFocusIn" ],
    [ "StickerIds", "structStickerIds.html", "structStickerIds" ]
];