var indexSectionsWithContent =
{
  0: "abcdefghijklmoprstu~",
  1: "abcefs",
  2: "aht",
  3: "abcdefhmrst",
  4: "bcdefghimoprsu~",
  5: "abcefijklstu",
  6: "i",
  7: "d",
  8: "fg",
  9: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines"
};

var indexSectionLabels =
{
  0: "Указатель",
  1: "Структуры данных",
  2: "Пространства имен",
  3: "Файлы",
  4: "Функции",
  5: "Переменные",
  6: "Определения типов",
  7: "Перечисления",
  8: "Элементы перечислений",
  9: "Макросы"
};

