var namespaces_dup =
[
    [ "actions", "namespaceactions.html", "namespaceactions" ],
    [ "handler", "namespacehandler.html", [
      [ "handleAnyMessage", "namespacehandler.html#a2e03d3cfa095c4e2f02537f09b823937", null ],
      [ "handleCommand", "namespacehandler.html#a9a6dcfadcc37d69d3dbec009faef3eb8", null ],
      [ "handleMyChatMember", "namespacehandler.html#ac9c509ee206604558dea3f27762e5dfc", null ],
      [ "handleTgBotException", "namespacehandler.html#afae451efc44275bab6823e6269f37b74", null ],
      [ "handleTgBotException", "namespacehandler.html#a6dc703b3fab49d1dbdfea217ada76075", null ]
    ] ],
    [ "telegram_api", "namespacetelegram__api.html", [
      [ "poll", "namespacetelegram__api.html#af73e27ad34081885c87294683c97739b", null ],
      [ "sendMessage", "namespacetelegram__api.html#a79eaeb6edfb3c9d39c05e0460cfde5e1", null ],
      [ "sendSticker", "namespacetelegram__api.html#a051d696acf025ab39b23e4f4c4c66838", null ]
    ] ]
];