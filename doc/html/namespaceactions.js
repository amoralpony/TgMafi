var namespaceactions =
[
    [ "Action", "classactions_1_1Action.html", "classactions_1_1Action" ],
    [ "ban", "namespaceactions.html#a211cf47418987def624c1d95a3f8690d", null ],
    [ "defineAction", "namespaceactions.html#a9365ce7b973679d16073390a35f70ed8", null ],
    [ "defineCommand", "namespaceactions.html#a9e76699a1e656b081c8d4739e0ec6b3a", null ],
    [ "ignore", "namespaceactions.html#a7141ec4218fef618781820c436d6b247", null ],
    [ "selectRandomSticker", "namespaceactions.html#a8f6fdb8a61743c5f24afe95f44bd040c", null ],
    [ "unfocus", "namespaceactions.html#accf9ae53e6a8d74d000653250a96fa2d", null ],
    [ "kActionsList", "namespaceactions.html#ab29072470aac59a4477fa8ad3000573c", null ],
    [ "kCommandsList", "namespaceactions.html#a83912644ee611088f494349c356c4b5a", null ]
];