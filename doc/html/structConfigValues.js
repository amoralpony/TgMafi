var structConfigValues =
[
    [ "botToken", "structConfigValues.html#a142f599c935b4fc63daa8042321ba054", null ],
    [ "canBanTrustedUserForOffense", "structConfigValues.html#a4a5db764d4761aee66c79043d91442c4", null ],
    [ "enableBanForOffense", "structConfigValues.html#a759f0f1ddbf710ec9803266fce7e9d97", null ],
    [ "focusRequiredForRandomAnswer", "structConfigValues.html#a0fef8aa3128213930075001dae64440c", null ],
    [ "logDebugInfo", "structConfigValues.html#a175bb98a99a2bd75baa283449c64e9cd", null ],
    [ "logPattern", "structConfigValues.html#aa59e22861718798659d6c27fe66a94f7", null ],
    [ "sendDecodedMessageToPrivateChat", "structConfigValues.html#a73ae9438e89093040da5516f4acf6850", null ],
    [ "stickerIds", "structConfigValues.html#a1c9102e0e9653abbda85f67a4629b497", null ],
    [ "trustedUserRequiredForDecode", "structConfigValues.html#a185afe3b5480e332ac6f3a16e9e2f34a", null ],
    [ "trustedUsers", "structConfigValues.html#a5593dd9a409241bd59201d52b7393258", null ]
];