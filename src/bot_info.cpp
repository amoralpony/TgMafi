#include "bot_info.hpp"

#include <regex>
#include <spdlog/spdlog.h>

/**
 * @brief Локальная функция, проверяющая, является ли токен бота корректным.
 * @details Токен Telegram-бота должен соответствовать такому паттерну: 10 любых
 * чисел, двоеточие, 35 буквенных или числовых символов или тире.
 *
 * @param[in] token Токен бота.
 * @return Возвращает значение, указывающее на корректность токена.
 */
static bool isTokenCorrect(const std::string& token)
{
  return std::regex_match(token, std::regex("^\\d{10}:([\\w-]){35}$"));
}

bool FocusIn::operator<(const FocusIn& rhs) const
{
  return std::tie(chatId, userId, focusVariant)
         < std::tie(rhs.chatId, rhs.userId, rhs.focusVariant);
}

BotInfo::BotInfo(const std::string& token): bot(TgBot::Bot(token))
{
  if (!isTokenCorrect(token))
  {
    spdlog::error("Token is incorrect: '{}'. Exit with code 1.", token);
    exit(1);
  }
}

BotInfo::BotInfo(const BotInfo& botInfo): bot(botInfo.bot.getToken())
{
  focusMap = botInfo.focusMap;
  stickers = botInfo.stickers;
  trustedUsers = botInfo.trustedUsers;
}
