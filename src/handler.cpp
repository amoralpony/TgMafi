#include "handler.hpp"

#include "action_definitions.hpp"
#include "bot_info.hpp"
#include "telegram_api.hpp"

#include <spdlog/spdlog.h>
#include <string>
#include <tgbot/TgException.h>
#include <thread>

namespace handler
{
  void handleAnyMessage(BotInfo& botInfo)
  {
    botInfo.bot.getEvents().onAnyMessage(
      [&botInfo](TgBot::Message::Ptr message)
      {
        // TODO: отрефакторить многопоточный код (если так можно).
        std::thread(
          [&botInfo, message]
          {
            if (message->chat->type != TgBot::Chat::Type::Private)
            {
              spdlog::info("Sended message '{}' (messageID: {}) in chat '{}' "
                           "(chatID: {}) by @{} (userID: {}).",
                           message->text, message->messageId,
                           message->chat->title, message->chat->id,
                           message->from->username, message->from->id);

              actions::Action* pAction = actions::defineAction(message->text);

              if (pAction)
              {
                spdlog::info(
                  "------------[ Message '{}' (messageID: {}) in chat '{}' "
                  "(chatID: {}) by @{} (userID: {}) is action ]------------",
                  message->text, message->messageId, message->chat->title,
                  message->chat->id, message->from->username,
                  message->from->id);

                const std::string kActionName
                  = pAction->execute(botInfo, message);
                spdlog::info(
                  "------------[ Action of message '{}' (messageID: {}) in "
                  "chat '{}' (chatID: {}) by @{} (userID: {}) is defined: {} "
                  "]------------",
                  message->text, message->messageId, message->chat->title,
                  message->chat->id, message->from->username, message->from->id,
                  kActionName);
              }
            }
            else if (message->chat->type == TgBot::Chat::Type::Private)
            {
              spdlog::info(
                "Sended message '{}' in private chat with @{} (userID: {}).",
                message->text, message->from->username, message->from->id);
            }
          })
          .detach();
      });
  }

  void handleCommand(BotInfo& botInfo)
  {
    std::unordered_map<std::string, actions::Action*>::const_iterator
      commandsIterator
      = actions::kCommandsList.begin();

    for (std::uint8_t i = 0; i < actions::kCommandsList.size(); i++)
    {
      botInfo.bot.getEvents().onCommand(
        commandsIterator->first,
        [&botInfo](TgBot::Message::Ptr message)
        {
          // TODO: отрефакторить многопоточный код (если так можно).
          std::thread(
            [&botInfo, message]
            {
              if (message->chat->type == TgBot::Chat::Type::Private)
              {
                actions::Action* pCommand
                  = actions::defineCommand(message->text);

                if (pCommand)
                {
                  spdlog::info(
                    "------------[ Message '{}' (messageID: {}) in private "
                    "chat with @{} (userID: {}) is command ]------------",
                    message->text, message->messageId, message->from->username,
                    message->from->id);

                  const std::string kCommandName
                    = pCommand->execute(botInfo, message);
                  spdlog::info(
                    "------------[ Command of message '{}' (messageID: {}) in "
                    "private chat with @{} (userID: {}) is defined: {} "
                    "]------------",
                    message->text, message->messageId, message->from->username,
                    message->from->id, kCommandName);
                }
              }
            })
            .detach();
        });

      commandsIterator++;
    }
  }

  void handleMyChatMember(BotInfo& botInfo)
  {
    botInfo.bot.getEvents().onMyChatMember(
      [&botInfo](TgBot::ChatMemberUpdated::Ptr member)
      {
        if (member->newChatMember->status == "member"
            || member->newChatMember->status == "administrator")
        {
          telegram_api::sendSticker(
            botInfo.bot, member->chat->id,
            actions::selectRandomSticker(botInfo.stickers.joinToGroup));
        }
      });
  }

  void handleTgBotException(const std::exception& exception)
  {
    spdlog::error("Exception at tgbot-cpp library:");
    spdlog::trace(exception.what());
  }

  void handleTgBotException(const std::exception& exception,
                            TgBot::TgLongPoll& pollObject)
  {
    handleTgBotException(exception);
    telegram_api::poll(pollObject);
  }
} // namespace handler
