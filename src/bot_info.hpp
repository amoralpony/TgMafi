#pragma once

#include "sticker_ids.hpp"

#include <map>
#include <string>
#include <tgbot/Bot.h>
#include <vector>

/**
 * @brief Структура, содержащая идентификаторы, требуемые для привязки фокуса к
 * конкретному пользователю в чате.
 */
struct FocusIn
{
  /**
   * @brief Поле, содержащее ID чата.
   */
  std::int64_t chatId;

  /**
   * @brief Поле, содержащее ID пользователя.
   */
  std::int64_t userId;

  /**
   * @brief Поле, содержащее тип фокуса.
   * @li 0 - э, мафи, э...;
   * @li 1 - ты, иди сюда;
   *
   * @note Если Вы добавляете новый тип, то измените значение переменной
   * kFocusTypeCount в функции @link actions::unfocus() @endlink на количество
   * значений фокуса.
   */
  std::uint8_t focusVariant;

  /**
   * @brief Оператор, требуемый @c std::map для хеширования. Нужен для
   * использования этой структуры как ключа карты.
   */
  bool operator<(const FocusIn& rhs) const;
};

/**
 * @brief Структура, содержащая информацию и состояние бота. На этой структуре
 * завязан весь код.
 */
struct BotInfo
{
public:
  /**
   * @brief Поле, содержащее объект бота.
   */
  TgBot::Bot bot;

  /**
   * @brief Поле, содержащее состояние фокуса для каждого пользователя в
   * отдельном чате.
   */
  std::map<FocusIn, bool> focusMap;

  /**
   * @brief Поле, содержащее ID стикеров, используемые ботом.
   */
  StickerIds stickers;

  /**
   * @brief Поле, содержащее список (вектор) доверенных пользователей.
   */
  std::vector<std::string> trustedUsers;

  /**
   * @brief Конструктор, инициализирующий эту структуру токеном бота.
   *
   * @param[in] token Токен бота.
   */
  BotInfo(const std::string& token);

  /**
   * @brief Конструктор копирования. Явно удалён, т.к. @c TgBot::Bot имеет явно
   * удалённый конструктор копирования.
   */
  BotInfo(const BotInfo& botInfo);
};
