#include "telegram_api.hpp"

#include "handler.hpp"

#include <boost/system/system_error.hpp>
#include <spdlog/spdlog.h>
#include <tgbot/TgException.h>

namespace telegram_api
{
  void poll(TgBot::TgLongPoll& pollObject)
  {
    try
    {
      while (true)
      {
        spdlog::debug("Poll started.");
        pollObject.start();
      }
    }
    catch (boost::wrapexcept<boost::system::system_error>& e)
    {
      spdlog::error("Host not found, check your internet connection.");
      poll(pollObject);
    }
  }

  void sendMessage(const TgBot::Bot& bot, const std::int64_t& chatId,
                   const std::string message,
                   const std::int64_t& replyToMessage)
  {
    if (bot.getApi().blockedByUser(chatId))
    {
      spdlog::error(
        "Bot is blocked by user. Message '{}' to chat '{}' hasn't been sent.",
        message, chatId);
      return;
    }

    try
    {
      bot.getApi().sendMessage(chatId, message, 0,
                               replyToMessage ? replyToMessage : 0);
    }
    catch (TgBot::TgException& e)
    {
      handler::handleTgBotException(e);
    }
  }

  void sendSticker(const TgBot::Bot& bot, const std::int64_t& chatId,
                   const std::string& stickerId,
                   const std::int64_t& replyToMessage)
  {
    try
    {
      bot.getApi().sendSticker(chatId, stickerId,
                               replyToMessage ? replyToMessage : 0);
    }
    catch (TgBot::TgException& e)
    {
      handler::handleTgBotException(e);
    }
  }
} // namespace telegram_api
