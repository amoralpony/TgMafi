#include "config_manager.hpp"

#include <fstream>
#include <regex>
#include <spdlog/spdlog.h>
#include <thread>

bool ConfigManager::isOnConfigUpdatePreviouslyCalled_ = false;

/**
 * @brief Локальная функция, создающая регулярное выражение, определяющее,
 * является ли строка конфигурационного файла параметром.
 *
 * @param[in] key Ключ.
 * @return Регулярное выражение, определяющее, является ли строка
 * конфигурационного файла параметром.
 */
static std::regex buildParameterRegex(const std::string& key)
{
  // On Windows: error G60A5EC10: no member named 'multiline' in
  // 'std::basic_regex<char>'
#ifdef _WIN32_WINNT
  return std::regex('^' + key + "=.*");
#else
  return std::regex('^' + key + "=.*", std::regex::multiline);
#endif
}

std::string ConfigManager::getValue_(const std::string key) const
{
  std::smatch lineWithParameter;
  if (std::regex_search(configContent_, lineWithParameter,
                        buildParameterRegex(key)))
  {
    const std::string kValue = lineWithParameter.str().substr(key.length() + 1);
    spdlog::info(
      "Found the '{}' parameter in the configuration file with value '{}'.",
      key, kValue);
    return kValue;
  }
  else
  {
    spdlog::warn("The '{}' parameter in the configuration file wasn't found.",
                 key);
  }

  return "";
}

std::vector<std::string> ConfigManager::getValues_(const std::string key) const
{
  std::vector<std::string> tmpResult;

  const std::regex kRegex = buildParameterRegex(key);
  std::sregex_iterator i = std::sregex_iterator(configContent_.begin(),
                                                configContent_.end(), kRegex);

  if (!i->size())
  {
    spdlog::warn(
      "The '{}' parameter (set) in the configuration file wasn't found.", key);
    return {};
  }

  for (std::sregex_iterator i = std::sregex_iterator(
         configContent_.begin(), configContent_.end(), kRegex);
       i != std::sregex_iterator(); i++)
  {
    const std::string kValue = i->str().substr(key.length() + 1);
    spdlog::info("Found the '{}' parameter (set) in the configuration file "
                 "with value '{}'.",
                 key, kValue);
    if (!kValue.empty())
    {
      tmpResult.push_back(kValue);
    }
  }

  return tmpResult;
}

void ConfigManager::updateValues_()
{
  spdlog::info(
    "------------[ Parsing values in configuration file ]------------");

  currentValues.canBanTrustedUserForOffense
    = getValue_("CAN_BAN_TRUSTED_USER_FOR_OFFENSE") == "TRUE";
  currentValues.enableBanForOffense
    = getValue_("ENABLE_BAN_FOR_OFFENSE") == "TRUE";
  currentValues.focusRequiredForRandomAnswer
    = getValue_("FOCUS_REQUIRED_FOR_RANDOM_ANSWER") == "TRUE";
  currentValues.logDebugInfo = getValue_("LOG_DEBUG_INFO") == "TRUE";
  currentValues.logPattern = getValue_("LOG_PATTERN");
  currentValues.trustedUsers = getValues_("TRUSTED_USER");
  currentValues.trustedUserRequiredForDecode
    = getValue_("TRUSTED_USER_REQUIRED_FOR_DECODE") == "TRUE";
  currentValues.sendDecodedMessageToPrivateChat
    = getValue_("SEND_DECODED_MESSAGE_TO_PRIVATE_CHAT") == "TRUE";

  currentValues.stickerIds.aggressiveWalk
    = getValues_("STICKER_AGGRESSIVE_WALK");
  currentValues.stickerIds.answerNo = getValues_("STICKER_ANSWER_NO");
  currentValues.stickerIds.answerYes = getValues_("STICKER_ANSWER_YES");
  currentValues.stickerIds.banChatMember
    = getValues_("STICKER_BAN_CHAT_MEMBER");
  currentValues.stickerIds.error = getValues_("STICKER_ERROR");
  currentValues.stickerIds.focusVariant0
    = getValues_("STICKER_FOCUS_VARIANT_0");
  currentValues.stickerIds.focusVariant1
    = getValues_("STICKER_FOCUS_VARIANT_1");
  currentValues.stickerIds.ignore = getValues_("STICKER_IGNORE");
  currentValues.stickerIds.joinToGroup = getValues_("STICKER_JOIN_TO_GROUP");
  currentValues.stickerIds.siren = getValues_("STICKER_SIREN");

  spdlog::info(
    "------------[ End of parsing values in configuration file ]------------");
}

std::string ConfigManager::readConfig_(bool enableLogging)
{
  std::ifstream configFileStream = std::ifstream(configPath_);
  std::string tmpResult;

  if (configFileStream.is_open())
  {
    if (enableLogging)
    {
      spdlog::debug("Configuration file has been successfully opened.");
    }

    char tmpChar;
    while (configFileStream.get(tmpChar))
    {
      tmpResult += tmpChar;
    }
  }
  else if (enableLogging)
  {
    spdlog::error("Error ocurred while opening configuration file.");
  }

  return tmpResult;
}

void ConfigManager::onUpdate(std::function<void()> action)
{
  if (isOnConfigUpdatePreviouslyCalled_)
  {
    spdlog::warn(
      "Method 'ConfigManager::onConfigUpdate' is called more than 1 time.");
  }
  isOnConfigUpdatePreviouslyCalled_ = true;

  // При вызове этого метода также происходит инициализация значений из
  // конфигурационного файла.
  action();

  std::thread(
    [this, action](std::string& configContent)
    {
      std::string newConfigContent;
      while (true)
      {
        // TODO: по-другому реализовать этот цикл (если так можно).
        std::this_thread::sleep_for(std::chrono::seconds(5));

        newConfigContent = readConfig_(false);
        if (configContent != newConfigContent && !newConfigContent.empty())
        {
          configContent = newConfigContent;
          updateValues_();
          action();
        }
      }
    },
    std::ref(configContent_))
    .detach();
}

ConfigManager::ConfigManager(const std::string configPath):
    configPath_(configPath)
{
  configContent_ = readConfig_();
  currentValues.botToken = getValue_("BOT_TOKEN");
  updateValues_();
}
