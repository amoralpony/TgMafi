#include "action_definitions.hpp"
#include "telegram_api.hpp"

#include <algorithm>
#include <bitset>
#include <spdlog/spdlog.h>
#include <unicode/unistr.h>

/**
 * @brief Локальное перечисление, содержащее методы раскодировки.
 */
enum DecodeMethod
{
  /**
   * @brief Сообщение представлено в двоичном коде, и его нужно перевести в
   * текст UTF-8.
   */
  FROM_BINARY_TO_UTF8,

  /**
   * @brief Сообщение написано на английской раскладке, и его нужно перевести на
   * русскую.
   */
  FROM_ENGLISH_TO_RUSSIAN_LAYOUT,

  /**
   * @brief Сообщение является стикером и нужно получить его ID.
   */
  GET_STICKER_ID
};

/**
 * @brief Локальная функция, определяющая, как надо раскодировать @c message.
 *
 * @param[in] message Структура сообщения.
 * @return Метод раскодировки сообщения.
 */
static DecodeMethod defineDecodeMethod(const TgBot::Message::Ptr& message)
{
  DecodeMethod tmpDecodeMethod;
  const bool kIsMessageBinaryCode
    = std::all_of(message->text.begin(), message->text.end(),
                  [](char symbol)
                  {
                    return symbol == '0' || symbol == '1' || symbol == ' ';
                  })
      && !message->text.empty();
  if (kIsMessageBinaryCode)
  {
    tmpDecodeMethod = FROM_BINARY_TO_UTF8;
  }
  else if (message->sticker)
  {
    tmpDecodeMethod = GET_STICKER_ID;
  }
  else
  {
    tmpDecodeMethod = FROM_ENGLISH_TO_RUSSIAN_LAYOUT;
  }

  spdlog::info("Decode method is defined: {}.", (std::uint8_t)tmpDecodeMethod);
  return tmpDecodeMethod;
}

/**
 * @brief Локальная функция, преобразующая строку, содержащую двоичный код в
 * строку UTF-8. Поддерживает пробел как разделитель.
 *
 * @param[in] text Строка с двоичным кодом
 * @return Раскодированная строка UTF-8.
 */
static std::string fromBinaryToUtf8(std::string text)
{
  text.erase(std::remove_if(text.begin(), text.end(),
                            [](unsigned char symbol)
                            {
                              return symbol == ' ';
                            }),
             text.end());

  std::string tmpDecodedText;
  for (size_t i = 0; i < text.length() / 8; i++)
  {
    tmpDecodedText
      += (char)std::bitset<8>(text.substr(i * 8, i * 8 + 8)).to_ulong();
  }

  return tmpDecodedText;
}

/**
 * @brief Локальная функция, преобразующая текст, написанный на английской
 * раскладке клавиатуры в русскую раскладку.
 *
 * @param[in] text Исходный текст.
 * @return Текст, преобразованный в русскую раскладку клавиатуры.
 */
static std::string fromEnglishToRussianLayout(const std::string& text)
{
  const std::unordered_map<char, std::string> kEnglishAndRussianLetters
    = {{'`', "ё"}, {'~', "Ё"}, {'[', "х"},  {'{', "Х"}, {']', "ъ"}, {'}', "Ъ"},
       {';', "ж"}, {':', "Ж"}, {'\'', "э"}, {'"', "Э"}, {',', "б"}, {'<', "Б"},
       {'.', "ю"}, {'>', "Ю"}, {'/', "."},  {'?', ","}, {'q', "й"}, {'Q', "Й"},
       {'w', "ц"}, {'W', "Ц"}, {'e', "у"},  {'E', "У"}, {'r', "к"}, {'R', "К"},
       {'t', "е"}, {'T', "Е"}, {'y', "н"},  {'Y', "Н"}, {'u', "г"}, {'U', "Г"},
       {'i', "ш"}, {'I', "Ш"}, {'o', "щ"},  {'O', "Щ"}, {'p', "з"}, {'P', "З"},
       {'a', "ф"}, {'A', "Ф"}, {'s', "ы"},  {'S', "Ы"}, {'d', "в"}, {'D', "В"},
       {'f', "а"}, {'F', "А"}, {'g', "п"},  {'G', "П"}, {'h', "р"}, {'H', "Р"},
       {'j', "о"}, {'J', "О"}, {'k', "л"},  {'K', "Л"}, {'l', "д"}, {'L', "Д"},
       {'z', "я"}, {'Z', "Я"}, {'x', "ч"},  {'X', "Ч"}, {'c', "с"}, {'C', "С"},
       {'v', "м"}, {'V', "М"}, {'b', "и"},  {'B', "И"}, {'n', "т"}, {'N', "Т"},
       {'m', "ь"}, {'M', "Ь"}};

  std::string tmpDecodedText;
  for (const char& symbol: text)
  {
    if (kEnglishAndRussianLetters.find(symbol)
        != kEnglishAndRussianLetters.end())
    {
      tmpDecodedText += kEnglishAndRussianLetters.at(symbol);
    }
    else
    {
      tmpDecodedText += symbol;
    }
  }

  return tmpDecodedText;
}

/**
 * @brief Локальная функция, определяющая, является ли @c text строкой,
 * закодированной в UTF-8.
 *
 * @param[in] text Проверяющийся текст.
 * @return Закодирован ли @c text в UTF-8.
 */
static bool isUtf8(const std::string& text)
{
  if (text.empty())
  {
    return false;
  }

  const icu::UnicodeString kTextInUnicode = icu::UnicodeString::fromUTF8(text);
  std::string utf8;
  kTextInUnicode.toUTF8String(utf8);

  return utf8 == text;
}

std::string actions::Decode::execute(BotInfo& botInfo,
                                     const TgBot::Message::Ptr& message)
{
  ExecuteConditions conditions(botInfo, message);
  if (!conditions.isMessageIsReply())
  {
    return "Decode (incorrect call)";
  }

  if (!conditions.isUserIsTrusted(message->from)
      && conditions.settings.trustedUserRequiredForDecode)
  {
    return ignore(botInfo, message);
  }

  std::string decodedMessage;
  switch (defineDecodeMethod(message->replyToMessage))
  {
  case FROM_BINARY_TO_UTF8:
    decodedMessage = fromBinaryToUtf8(message->replyToMessage->text);
    break;
  case FROM_ENGLISH_TO_RUSSIAN_LAYOUT:
    decodedMessage = fromEnglishToRussianLayout(message->replyToMessage->text);
    break;
  case GET_STICKER_ID:
    decodedMessage = message->replyToMessage->sticker->fileId;
  }

  spdlog::info("Given message: '{}'; Decoded message: '{}'.",
               message->replyToMessage->text, decodedMessage);

  if (isUtf8(decodedMessage))
  {
    if (conditions.settings.sendDecodedMessageToPrivateChat)
    {
      telegram_api::sendMessage(botInfo.bot, message->from->id, decodedMessage);
    }
    else
    {
      telegram_api::sendMessage(botInfo.bot, message->chat->id, decodedMessage,
                                message->messageId);
    }
  }
  else
  {
    telegram_api::sendSticker(botInfo.bot, message->chat->id,
                              selectRandomSticker(botInfo.stickers.error),
                              message->messageId);
    spdlog::error("String encoding isn't UTF-8. Skipping sending a message.");
  }

  return "Decode";
}
