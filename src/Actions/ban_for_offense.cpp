#include "action_definitions.hpp"

#include <spdlog/spdlog.h>
#include <unicode/unistr.h>

/**
 * @brief Локальная функция, сравнивающая две Unicode строки, игнорируя регистр.
 *
 * @param[in] first Первая сравниваемая строка.
 * @param[in] second Вторая сравниваемая строка.
 * @return Равны ли @c first и @c second, игнорируя регистр.
 */
static bool compareUnicodeStringsICase(const std::string first,
                                       const std::string second)
{
  if (first.size() != second.size())
  {
    return false;
  }

  return icu::UnicodeString(first.c_str()).toLower()
         == icu::UnicodeString(second.c_str()).toLower();
}

std::string actions::BanForOffense::execute(BotInfo& botInfo,
                                            const TgBot::Message::Ptr& message)
{
  ExecuteConditions conditions(botInfo, message);
  if (!conditions.settings.enableBanForOffense || !conditions.isIIsAdmin()
      || conditions.isUserIsAdmin(message->from)
      || (conditions.isUserIsTrusted(message->from)
          && !conditions.settings.canBanTrustedUserForOffense)
      || !((conditions.isMessageIsReply()
            && conditions.isUserIsMe(message->replyToMessage->from))
           || compareUnicodeStringsICase("мафи", message->text.substr(0, 8))))
  {
    return "Ban for offense (not for Mafi)";
  }

  ban(botInfo, message, message->from);
  return "Ban for offense";
}
