#include "action_definitions.hpp"
#include "telegram_api.hpp"

#include <cstdlib>
#include <spdlog/spdlog.h>

std::string actions::RandomAnswer::execute(BotInfo& botInfo,
                                           const TgBot::Message::Ptr& message)
{
  ExecuteConditions conditions(botInfo, message);
  if (!conditions.hasFocusWithVariant(1)
      && conditions.settings.focusRequiredForRandomAnswer)
  {
    return ignore(botInfo, message);
  }

  const bool kRandomAnswer = rand() % 2 == 0;

  telegram_api::sendSticker(botInfo.bot, message->chat->id,
                            selectRandomSticker(kRandomAnswer ?
                                                  botInfo.stickers.answerYes :
                                                  botInfo.stickers.answerNo),
                            message->messageId);
  spdlog::info("Generated random answer: {}", kRandomAnswer);

  unfocus(botInfo, message);
  return "RandomAnswer";
}
