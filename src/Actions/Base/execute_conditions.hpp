#pragma once

#include "bot_info.hpp"
#include "config_manager.hpp"

#include <tgbot/types/Message.h>
#include <tgbot/types/User.h>

/**
 * @brief Пространство имён, содержащее условия для выполнения действий.
 */
class ExecuteConditions
{
private:
  /**
   * @brief Поле, содержащее информацию о боте. Используется для получения карты
   * фокусов и доверенных пользователей.
   */
  BotInfo botInfo_;

  /**
   * @brief Поле, содержащее последнее сообщение, предоставляющее информацию об
   * отправителе, о самом сообщении.
   */
  const TgBot::Message::Ptr lastMessage_;

public:
  /**
   * @brief Поле, содержащее настройки (значения конфигурационного файла) бота.
   */
  static inline ConfigValues settings;

  /**
   * @brief Метод, проверяющий, есть ли у пользователя, отправившего последнее
   * сообщение фокус с вариантом @c focusVariant.
   *
   * @param[in] focusVariant Вариант фокуса.
   * @return Есть ли у пользователя, отправившего последнее сообщение фокус с
   * вариантом @c focusVariant.
   */
  bool hasFocusWithVariant(const std::uint8_t focusVariant);

  /**
   * @brief Метод, проверяющий, является ли бот администратором.
   *
   * @return Является ли бот администратором.
   */
  bool isIIsAdmin();

  /**
   * @brief Метод, проверяющий, является ли @c user администратором или
   * владельцом чата, в котором отправлено последнее сообщение.
   *
   * @param[in] user Структура пользователя.
   * @return Является ли @c user администратором или владельцом чата.
   */
  bool isUserIsAdmin(const TgBot::User::Ptr& user);

  /**
   * @brief Метод, проверяющий, является ли @c user этим ботом.
   *
   * @param[in] user Структура пользователя.
   * @return Является ли @c user этим ботом.
   */
  bool isUserIsMe(const TgBot::User::Ptr& user);

  /**
   * @brief Метод, проверяющий, является ли последнее сообщение ответом на
   * другое сообщение.
   *
   * @return Является ли последнее сообщение ответом на другое сообщение.
   */
  bool isMessageIsReply();

  /**
   * @brief Метод, проверяющий, является ли user доверенным пользователем.
   *
   * @note Если @c botInfo_.trustedUsers пустой, то в любом случае возвращается
   * @c true.
   *
   * @param[in] user Проверяемый пользователь.
   * @return Является ли @c user доверенным пользователем.
   */
  bool isUserIsTrusted(const TgBot::User::Ptr& user);

  /**
   * @brief Конструктор @c ExecuteConditions. Инициализирует этот класс
   * информацией сообщения и бота.
   *
   * @param[in] botInfo Информация о боте.
   * @param[in] message Структура сообщения.
   */
  ExecuteConditions(const BotInfo& botInfo, const TgBot::Message::Ptr& message);
};
