#include "execute_conditions.hpp"

#include <spdlog/spdlog.h>

bool ExecuteConditions::hasFocusWithVariant(const std::uint8_t focusVariant)
{
  spdlog::debug(
    "Checking: is user @{} (userID: {}) has focus with variant {} in chat "
    "'{}' (chatID: {}).",
    lastMessage_->from->username, lastMessage_->from->id, focusVariant,
    lastMessage_->chat->title, lastMessage_->chat->id);

  const FocusIn kFocusLocation
    = {lastMessage_->chat->id, lastMessage_->from->id, focusVariant};
  if (botInfo_.focusMap.find(kFocusLocation) != botInfo_.focusMap.end())
  {
    return botInfo_.focusMap.at(kFocusLocation);
  }
  else
  {
    return false;
  }
}

bool ExecuteConditions::isIIsAdmin()
{
  spdlog::debug("Checking: is Mafi is admin.");
  return botInfo_.bot.getApi()
           .getChatMember(lastMessage_->chat->id,
                          botInfo_.bot.getApi().getMe()->id)
           ->status
         == "administrator";
}

bool ExecuteConditions::isUserIsAdmin(const TgBot::User::Ptr& user)
{
  spdlog::debug("Checking: is user @{} (userID: {}) is admin.", user->username,
                user->id);

  const std::string kReplyFromStatus
    = botInfo_.bot.getApi()
        .getChatMember(lastMessage_->chat->id, user->id)
        ->status;
  return kReplyFromStatus == "administrator" || kReplyFromStatus == "creator";
}

bool ExecuteConditions::isUserIsMe(const TgBot::User::Ptr& user)
{
  spdlog::debug("Checking: is user @{} (userID: {}) is bot (me).",
                user->username, user->id);

  return user->id == botInfo_.bot.getApi().getMe()->id;
}

bool ExecuteConditions::isMessageIsReply()
{
  spdlog::debug("Checking: is message '{}' (messageID: {}) is reply.",
                lastMessage_->text, lastMessage_->messageId);

  return lastMessage_->replyToMessage != nullptr;
}

bool ExecuteConditions::isUserIsTrusted(const TgBot::User::Ptr& user)
{
  spdlog::debug("Checking: is user @{} (userID: {}) trusted.", user->username,
                user->id);

  if (!botInfo_.trustedUsers.size())
  {
    return true;
  }

  return std::find(botInfo_.trustedUsers.begin(), botInfo_.trustedUsers.end(),
                   user->username)
         != std::end(botInfo_.trustedUsers);
}

ExecuteConditions::ExecuteConditions(const BotInfo& botInfo,
                                     const TgBot::Message::Ptr& message):
    botInfo_(botInfo),
    lastMessage_(message)
{
}
