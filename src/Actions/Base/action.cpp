#include "action.hpp"

#include "action_definitions.hpp"
#include "handler.hpp"
#include "telegram_api.hpp"

#include <boost/regex/icu.hpp>
#include <chrono>
#include <regex>
#include <spdlog/spdlog.h>

namespace actions
{
  void ban(const BotInfo& botInfo, const TgBot::Message::Ptr& message,
           const TgBot::User::Ptr& user)
  {
    const std::string kBanStickersOrder[]
      = {selectRandomSticker(botInfo.stickers.siren),
         selectRandomSticker(botInfo.stickers.aggressiveWalk),
         selectRandomSticker(botInfo.stickers.banChatMember)};

    for (const std::string& stickerId: kBanStickersOrder)
    {
      try
      {
        telegram_api::sendSticker(botInfo.bot, message->chat->id, stickerId);
      }
      catch (TgBot::TgException& e)
      {
        handler::handleTgBotException(e);
      }

      std::this_thread::sleep_for(std::chrono::seconds(3));
    }

    try
    {
      botInfo.bot.getApi().banChatMember(message->chat->id, user->id);

      spdlog::info("User @{} (userID: {}) banned in chat '{}' (chatID: {}).",
                   user->username, message->replyToMessage->from->id,
                   message->chat->title, message->chat->id);
    }
    catch (TgBot::TgException& e)
    {
      handler::handleTgBotException(e);
    }
  }

  Action* defineAction(const std::string message)
  {
    std::unordered_map<std::string, Action*>::const_iterator actionsIterator
      = kActionsList.begin();

    Action* action = nullptr;
    for (std::uint8_t i = 0; i < kActionsList.size(); i++)
    {
      // Здесь используется библиотека Boost, т.к. для игнорирования регистра
      // (boost::regex::icase) нужно включить поддержку Unicode, что в
      // стандартной библиотеке сильно раздует код.
      if (boost::u32regex_match(
            message.c_str(),
            boost::make_u32regex(actionsIterator->first.c_str(),
                                 boost::regex::icase)))
      {
        action = actionsIterator->second;
        break;
      }

      actionsIterator++;
    }

    return action;
  }

  Action* defineCommand(const std::string message)
  {
    std::unordered_map<std::string, Action*>::const_iterator commandsIterator
      = kCommandsList.begin();

    Action* command = nullptr;
    for (std::uint8_t i = 0; i < kCommandsList.size(); i++)
    {
      if (message.substr(1) == commandsIterator->first)
      {
        command = commandsIterator->second;
        break;
      }

      commandsIterator++;
    }

    return command;
  }

  std::string ignore(BotInfo& botInfo, const TgBot::Message::Ptr& message)
  {
    telegram_api::sendSticker(botInfo.bot, message->chat->id,
                              selectRandomSticker(botInfo.stickers.ignore),
                              message->messageId);
    return "Ignore";
  }

  std::string selectRandomSticker(const std::vector<std::string>& stickers)
  {
    if (stickers.size() == 1)
    {
      return stickers.at(0);
    }

    return stickers.at(rand() % stickers.size());
  }

  void unfocus(BotInfo& botInfo, const TgBot::Message::Ptr& message)
  {
    constexpr std::uint8_t kFocusTypeCount = 2;
    for (std::uint8_t i = 0; i < kFocusTypeCount; i++)
    {
      botInfo.focusMap.erase(
        {message->chat->id, message->from->id, kFocusTypeCount});
    }

    spdlog::debug(
      "Focus for user @{} (userID: {}) in chat '{}' (chatID: {}) is false.",
      message->from->username, message->from->id, message->chat->title,
      message->chat->id);
  }
} // namespace actions
