#pragma once

#include "bot_info.hpp"

#include <string>
#include <tgbot/types/Message.h>

/**
 * @brief Пространство имён, содержащее в себе всё необходимое для работы с
 * действиями и командами.
 *
 * @note Команда является действием.
 */
namespace actions
{
  /**
   * @brief Абстрактный базовый класс для всех действий.
   */
  class Action
  {
  public:
    /**
     * @brief Деструктор класса-действия. Используется для освобождения памяти
     * действия.
     */
    virtual ~Action() = default;

    /**
     * @brief Метод, содержащий реализацию действия.
     *
     * @note Действия не должны зависеть друг от друга. Поэтому такие действия,
     * как @link actions::ignore() @endlink и @link actions::unfocus() @endlink
     * нужно выносить в пространство имён @link actions @endlink как функции.
     *
     * @param[in, out] botInfo Информация о боте.
     * @param[in] message Структура сообщения.
     * @return Имя действия. Используется для логирования.
     *
     * Пример реализации:
     * @code
     * std::string actions::MyAction::execute(BotInfo& botInfo, const
     *                                        TgBot::Message::Ptr& message)
     * {
     *   if (rand() % 2)
     *   {
     *     telegram_api::sendMessage(
     *       botInfo.bot, message->chat->id, "Анви - мать.");
     *   }
     *   else
     *   {
     *     return ignore(botInfo, message);
     *   }
     *
     *   return "MyAction";
     * }
     * @endcode
     */
    virtual std::string execute(BotInfo& botInfo,
                                const TgBot::Message::Ptr& message)
      = 0;
  };

  /**
   * @brief Функция, банящая пользователя.
   *
   * @param[in] botInfo Информация о боте.
   * @param[in] message Структура сообщения.
   * @param[in] user Структура пользователя, который будет забанен.
   */
  void ban(const BotInfo& botInfo, const TgBot::Message::Ptr& message,
           const TgBot::User::Ptr& user);

  /**
   * @brief Функция, определяющая действие сообщения @c message.
   *
   * @param[in] message Сообщение.
   * @return Указатель на класс-действие.
   */
  Action* defineAction(const std::string message);

  /**
   * @brief Функция, определяющая действие (как команду) сообщения @c message.
   *
   * @param[in] message Сообщение.  * @return Указатель на класс-действие.
   */
  Action* defineCommand(const std::string message);

  /**
   * @brief Функция, содержащая реализацию действия, игнорирующего запрос (в
   * плане того, что игнорирует не программа, а бот).
   *
   * @param[in, out] botInfo Информация о боте.
   * @param[in] message Структура сообщения.
   * @return Имя действия (строка "Ignore").
   */
  std::string ignore(BotInfo& botInfo, const TgBot::Message::Ptr& message);

  /**
   * @brief Получает рандомный ID стикера из списка (вектора) стикеров.
   *
   * @param[in] stickers Список (вектор) стикеров.
   * @return Рандомный ID стикера.
   */
  std::string selectRandomSticker(const std::vector<std::string>& stickers);

  /**
   * @brief Стирает для пользователя, отправившего @c message фокус всех
   * вариантов.
   *
   * @param[in, out] botInfo Информация о боте.
   * @param[in] message Структура сообщения.
   */
  void unfocus(BotInfo& botInfo, const TgBot::Message::Ptr& message);
} // namespace actions
