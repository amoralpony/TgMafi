#pragma once

#include "action.hpp"
#include "execute_conditions.hpp"

#include <unordered_map>

/**
 * @brief Локальная функция, генерирующая регулярное выражение, которое
 * соответствует оскорблению.
 *
 * @return Регулярное выражение оскорбления.
 */
static std::string generateOffenseRegex()
{
  const std::string kOffenses[] = {"аутист(ка)?",
                                   "бляд((ун)|(ь))",
                                   "вагина",
                                   "влагалище",
                                   "гнида",
                                   "г[ао]вн((ище)|(о)|(юк))",
                                   "даун(ище)?",
                                   "дерьм((о)|(ище))",
                                   "дрочила",
                                   "дурак",
                                   "ебан(ат)?",
                                   "еблан(ище)?",
                                   "жал((ок)|(кий))",
                                   "кретин(ище)?",
                                   "манда",
                                   "манд[ао]вошка",
                                   "мраз((ота)|(ь))",
                                   "муд((ак)|(ила))",
                                   "ничтоже((н)|(ство))",
                                   "обезьянк?а",
                                   "осквернитель",
                                   "очко",
                                   "пердун",
                                   "пидо?р",
                                   "пид[ао]рас",
                                   "пидрила",
                                   "пизд[ао]бол",
                                   "пизда",
                                   "попуск",
                                   "примат((ище)|(ыш))?",
                                   "проститутка",
                                   "рукоблуд(ище)?",
                                   "cсанина",
                                   "суч?ка",
                                   "тупой",
                                   "у[её]б((ок)|(ище))",
                                   "уебан",
                                   "урод((ец)|(ище))?",
                                   "чепушил((а)|(ище))",
                                   "чмо(шник)?",
                                   "шавка",
                                   "шлак",
                                   "шлюха"};

  std::string tmpRegex = "((ты )|(мафи ))? (";
  for (const std::string& offense: kOffenses)
  {
    tmpRegex += '(' + offense
                + (offense == kOffenses[std::size(kOffenses) - 1] ? ")" : ")|");
  }
  tmpRegex += ") ?.*";

  return tmpRegex;
}

namespace actions
{
/**
 * @brief Макрос, определяющий действие @c name. Создан, чтобы не раздувать код
 * объявлениями.
 *
 * @param[in] name Имя действия как класса.
 */
#define DEFINE_ACTION(name) \
  class name: public Action \
  { \
    std::string execute(BotInfo& botInfo, \
                        const TgBot::Message::Ptr& message) override; \
  };

  // Действия
  DEFINE_ACTION(BanForOffense)
  DEFINE_ACTION(Decode)
  DEFINE_ACTION(FocusVariant0)
  DEFINE_ACTION(FocusVariant1)
  DEFINE_ACTION(RandomAnswer)
  DEFINE_ACTION(ReplyBan)

  // Команды
  DEFINE_ACTION(Start)

  /**
   * @brief Глобальная константа, определяющая регулярные выражения действий.
   * @details Эта константа имеет тип std::unordered_map<std::string, @link
   * Action
   * @endlink*>. @c std::string - это регулярное выражение, которое сравнивается
   * с каждым сообщением. Если сообщение будет подходить к одному регулярному
   * выражению, то оно становится действием. А @link Action @endlink* - это
   * указатель на класс-действие, который будет определён в @link
   * actions::defineAction() @endlink.
   */
  const std::unordered_map<std::string, Action*> kActionsList
    = {{generateOffenseRegex(), new BanForOffense()},
       {"^раскодируй!", new Decode()},
       {"^э, мафи, э\\.\\.\\.$", new FocusVariant0()},
       {"^ты,? иди сюда[!).]?", new FocusVariant1()},
       {"^скажи[- ]ка,? голубчик, то,? что ((он сказал)|(она сказала)) [-—] "
        "правда\\?$",
        new RandomAnswer()},
       {"^(мафи, принеси ((его)|(её)) голову, вс[её], жду, "
        "пока[)!\\.]?)|(м\\.\\.\\. хмхм\\.\\.\\.)|(убить,? мафи, слышишь\\? "
        "убить! (уничтожить!)?)",
        new ReplyBan()}};

  /**
   * @brief Глобальная константа, определяющая регулярные выражений команд.
   * @details Эта константа имеет тип std::unordered_map<std::string, @link
   * Action
   * @endlink*>. @c std::string - это регулярное выражение, которое сравнивается
   * с каждым сообщением. Если сообщение будет подходить к одному регулярному
   * выражению, то оно становится командой. А @link Action @endlink* - это
   * указатель на класс-действие, который будет определён в @link
   * actions::defineCommand() @endlink.
   */
  const std::unordered_map<std::string, Action*> kCommandsList
    = {{"start", new Start()}};
} // namespace actions
