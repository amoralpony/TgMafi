#include "action_definitions.hpp"
#include "handler.hpp"
#include "telegram_api.hpp"

#include <spdlog/spdlog.h>

// * Этот файл содержит реализацию следующих действий: FocusVariant0,
// * FocusVariant1.

/**
 * @brief Локальная функция, задающая фокус на пользователе в конкретном чате,
 * учитывающая переданные параметры.
 *
 * @param[in, out] botInfo Информация о боте.
 * @param[in] message Последнее сообщение.
 * @param[in] stickerId Стикер, который бот отправит при фокусе.
 * @param[in] focusVariant Тип фокуса.
 * @return Имя действия ("Focus (variant $focusVariant)").
 */
static const std::string focus(BotInfo& botInfo,
                               const TgBot::Message::Ptr& message,
                               const std::string stickerId,
                               const std::uint8_t focusVariant)
{
  if (!ExecuteConditions(botInfo, message).isUserIsTrusted(message->from))
  {
    return actions::ignore(botInfo, message);
  }

  try
  {
    telegram_api::sendSticker(botInfo.bot, message->chat->id, stickerId,
                              message->messageId);
  }
  catch (TgBot::TgException& e)
  {
    handler::handleTgBotException(e);
  }

  // Снимаем фокус остальных вариантов.
  actions::unfocus(botInfo, message);
  botInfo.focusMap.insert_or_assign(
    {message->chat->id, message->from->id, focusVariant}, true);

  spdlog::info("Focus for user @{} (userID: {}) in chat '{}' (chatID: {}) is "
               "true (variant {}).",
               message->from->username, message->from->id, message->chat->title,
               message->chat->id, focusVariant);

  return "Focus (variant " + std::to_string(focusVariant) + ")";
}

std::string actions::FocusVariant0::execute(BotInfo& botInfo,
                                            const TgBot::Message::Ptr& message)
{
  return focus(botInfo, message,
               selectRandomSticker(botInfo.stickers.focusVariant0), 0);
}

std::string actions::FocusVariant1::execute(BotInfo& botInfo,
                                            const TgBot::Message::Ptr& message)
{
  return focus(botInfo, message,
               selectRandomSticker(botInfo.stickers.focusVariant1), 1);
}
