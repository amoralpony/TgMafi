#include "action_definitions.hpp"
#include "handler.hpp"

#include <spdlog/spdlog.h>

std::string actions::ReplyBan::execute(BotInfo& botInfo,
                                       const TgBot::Message::Ptr& message)
{
  ExecuteConditions conditions(botInfo, message);
  if (!conditions.isMessageIsReply())
  {
    return "ReplyBan (incorrect call)";
  }

  if (!conditions.isIIsAdmin() || !conditions.isUserIsTrusted(message->from)
      || conditions.isUserIsAdmin(message->replyToMessage->from)
      || conditions.isUserIsMe(message->replyToMessage->from)
      || (message->text == "м... хмхм..."
          && !conditions.hasFocusWithVariant(1)))
  {
    return ignore(botInfo, message);
  }

  ban(botInfo, message, message->replyToMessage->from);

  unfocus(botInfo, message);
  return "ReplyBan";
}
