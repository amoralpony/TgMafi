#pragma once

#include <cstdint>
#include <string>
#include <tgbot/Bot.h>
#include <tgbot/net/TgLongPoll.h>

/**
 * @brief Пространство имён, содержащее перегруженные функции API Telegram'а,
 * например, @link telegram_api::sendMessage @endlink перехватывает исключения и
 * логирует их.
 */
namespace telegram_api
{
  /**
   * @brief Функция, отправляющая сообщение @c message в чат @c chatId от имени
   * бота @c bot.
   *
   * @param[in] bot Структура бота. Отправитель сообщения.
   * @param[in] chatId Чат, куда отправляется сообщение.
   * @param[in] message Отправляемое сообщение.
   * @param[in] replyToMessage Необязателен. ID сообщения, на которое ссылается
   * отправляемое сообщение.
   */
  void sendMessage(const TgBot::Bot& bot, const std::int64_t& chatId,
                   const std::string message,
                   const std::int64_t& replyToMessage = 0);

  /**
   * @brief Функция, отправляющая стикер с ID @c stickerId в чат @c chatId от
   * имени бота @c bot.
   *
   * @param[in] bot Структура бота. Отправитель стикера.
   * @param[in] chatId Чат, куда отправляется стикер.
   * @param[in] stickerId ID отправляемого стикера.
   * @param[in] replyToMessage Необязателен. ID сообщения, на которое ссылается
   * отправляемый стикер.
   */
  void sendSticker(const TgBot::Bot& bot, const std::int64_t& chatId,
                   const std::string& stickerId,
                   const std::int64_t& replyToMessage = 0);

  /**
   * @brief Функция, опрашивающая API Telegram'а.
   *
   * @param[in, out] pollObject Объект опросчика.
   */
  void poll(TgBot::TgLongPoll& pollObject);
} // namespace telegram_api
