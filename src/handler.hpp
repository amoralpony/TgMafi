#pragma once

#include "action.hpp"
#include "bot_info.hpp"

#include <chrono>
#include <tgbot/TgException.h>
#include <tgbot/net/TgLongPoll.h>
#include <tgbot/types/Message.h>

/**
 * @brief Пространство имён, содержащее функции, перехватывающие события
 * Telegram, исключения, и т.п.
 */
namespace handler
{
  /**
   * @brief Функция, задающая поведение для @c botInfo при отправке любого
   * сообщения.
   * @details Если сообщение подходит по паттерну любого элемента @link
   * actions::kActionsList @endlink, то оно считается действием.\nУсловия, при
   * которых будет перехватываться сообщение: @li Сообщение отправлено после
   * запуска бота - без этой проверки бот будет читать и перехватывать
   * сообщения, которые были отправлены до запуска бота. @li Тип чата \b не
   * является приватным - использовать действия нельзя в личных сообщениях.
   *
   * @param[in, out] botInfo Структура botInfo. Используется для доступа к боту,
   * следовательно и к API Telegram'а.
   */
  void handleAnyMessage(BotInfo& botInfo);

  /**
   * @brief Функция, задающая поведение для @c botInfo при отправке любой
   * команды.
   * @details Условия, при которых будет перехватываться команда: @li Команда
   * отправлена после запуска бота - без этой проверки бот будет читать и
   * перехватывать команды, которые были отправлены до запуска бота. @li Тип
   * чата является приватным - использовать команды можно только в личных
   * сообщениях.
   *
   * @param[in, out] botInfo Структура botInfo. Используется для доступа к боту,
   * следовательно и к API Telegram'а.
   */
  void handleCommand(BotInfo& botInfo);

  /**
   * @brief Функция, задающая поведение для @c botInfo при вступлении в группу.
   * @details Условия, при котором будет перехватываться вступление в группу:
   * @li Вступление в группу произошло после запуска бота - бот не будет
   * перехватывать вступление в группу, которое произошло до его запуска. @li
   * Статус бота является "member" либо "administrator" - иначе будет вызываться
   * исключение, гласящее, что бот был кикнут из группы, которое перехватить
   * можно только в @link main() @endlink.
   *
   * @warning Эта функция также перехватывает любые изменения бота. Это
   * планируется исправить в следующих версиях.
   *
   * @param[in, out] botInfo Структура botInfo. Используется для доступа к боту,
   * следовательно и к API Telegram'а.
   */
  void handleMyChatMember(BotInfo& botInfo);

  /**
   * @brief Функция, перехватывающая исключение @c TgBot::TgException.
   *
   * @param[in] exception Объект исключения.
   */
  void handleTgBotException(const std::exception& exception);

  /**
   * @brief Функция, перехватывающая исключение @c TgBot::TgException и
   * воспроизводящая переподключение.
   *
   * @param[in] exception Объект исключения.
   * @param[in, out] pollObject Объект опросчика.
   */
  void handleTgBotException(const std::exception& exception,
                            TgBot::TgLongPoll& pollObject);
} // namespace handler
