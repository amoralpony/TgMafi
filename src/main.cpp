// Неужели кто-то заглянул в исходный код?
// Простите, что наговнокодил, но этого бота ждут человек 10 и он пишется уже
// месяц.
// 2?

#ifdef _WIN32_WINNT
#include <Windows.h>
#endif

#include "bot_info.hpp"
#include "config_manager.hpp"
#include "execute_conditions.hpp"
#include "handler.hpp"
#include "telegram_api.hpp"

#include <spdlog/spdlog.h>
#include <tgbot/TgException.h>
#include <tgbot/net/TgLongPoll.h>

int main()
{
#ifdef _WIN32_WINNT
  // В командной строке Windows по умолчанию стоит не UTF-8.
  SetConsoleOutputCP(CP_UTF8);
#endif

  std::srand(time(nullptr));

  spdlog::info("------------[ Starting bot ]------------");

  spdlog::set_level(spdlog::level::trace);

  ConfigManager configManager("mafi.conf");
  BotInfo mafi(configManager.currentValues.botToken);
  spdlog::info("Bot initialized with token '{}'.",
               configManager.currentValues.botToken);

  const std::string kDefaultLogPattern = "[%Y-%m-%d %H:%M:%S.%e] [%^%l%$] %v";

  configManager.onUpdate(
    [&mafi, &configManager, kDefaultLogPattern]
    {
      spdlog::info("------------[ Updating configuration ]------------");

      spdlog::set_level(configManager.currentValues.logDebugInfo ?
                          spdlog::level::trace :
                          spdlog::level::info);
      spdlog::info("Updated log level.");

      const std::string kLogPattern = configManager.currentValues.logPattern;
      spdlog::set_pattern(kLogPattern.empty() ? kDefaultLogPattern :
                                                kLogPattern);
      spdlog::info("Updated pattern for logs.");

      ExecuteConditions::settings = configManager.currentValues;

      mafi.stickers = configManager.currentValues.stickerIds;
      spdlog::info("Updated stickers for bot.");

      mafi.trustedUsers = configManager.currentValues.trustedUsers;
      spdlog::info("Updated trusted users list.");

      spdlog::info("------------[ End of updating configuration ]------------");
    });

  mafi.bot.getApi().deleteWebhook(true);
  spdlog::info("Deleted webhook and last messages.");

  spdlog::info("------------[ End of starting bot ]------------");

  handler::handleCommand(mafi);
  handler::handleAnyMessage(mafi);
  handler::handleMyChatMember(mafi);

  TgBot::TgLongPoll pollObject(mafi.bot);
  try
  {
    telegram_api::poll(pollObject);
  }
  catch (TgBot::TgException& e)
  {
    handler::handleTgBotException(e, pollObject);
  }
}
