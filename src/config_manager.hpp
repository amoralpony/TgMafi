#pragma once

#include "sticker_ids.hpp"

#include <functional>
#include <string>
#include <vector>

/**
 * @brief Структура, содержащая пропарсенные значения конфигурационного файла.
 */
struct ConfigValues
{
  /**
   * @brief Поле, содержащее значение, разрешающее или запрещающее банить
   * доверенных пользователей за оскорбление.
   */
  bool canBanTrustedUserForOffense;

  /**
   * @brief Поле, содержащее токен бота. Не должен изменятся во время работы
   * программы.
   *
   * @note Не может изменятся во время работы программы.
   */
  std::string botToken;

  /**
   * @brief Поле, содержащее значение, разрешающее или запрещающая банить за
   * оскорбление.
   */
  bool enableBanForOffense;

  /**
   * @brief Поле, содержащее значение, разрешающее или запрещающее получать
   * рандомный ответ без фокуса.
   */
  bool focusRequiredForRandomAnswer;

  /**
   * @brief Поле, содержащее значение, разрешающее или запрещающее логировать
   * отладочную информацию.
   */
  bool logDebugInfo;

  /**
   * @brief Поле, содержащее паттерн для логов (spdlog).
   */
  std::string logPattern;

  /**
   * @brief Поле, содержащее значение, указывающее, следует ли отправлять
   * раскодированное сообщение в приватный чат.
   */
  bool sendDecodedMessageToPrivateChat;

  /**
   * @brief Поле, содержащее ID всех стикеров.
   */
  StickerIds stickerIds;

  /**
   * @brief Поле, содержащее список (вектор) доверенных пользователей.
   */
  std::vector<std::string> trustedUsers;

  /**
   * @brief Поле, содержащее значение, разрешающее или запрещающее раскодировать
   * не доверенному пользователю.
   */
  bool trustedUserRequiredForDecode;
};

/**
 * @brief Класс, отвечающий за парсинг и управление конфигурационным файлом.
 */
class ConfigManager
{
private:
  /**
   * @brief Статическое поле, указывающее, был ли хоть раз вызван метод
   * @link ConfigManager::onConfigUpdate() @endlink.
   */
  static bool isOnConfigUpdatePreviouslyCalled_;

  /**
   * @brief Поле, содержащее содержимое конфигурационного файла.
   */
  std::string configContent_;

  /**
   * @brief Поле, содержащее относительный путь к файлу конфигурации.
   */
  std::string configPath_;

  /**
   * @brief Метод, парсящий значение по ключу @c key.
   *
   * @param[in] key Ключ.
   * @return Значение по ключу.
   */
  std::string getValue_(const std::string key) const;

  /**
   * @brief Метод, парсящий значения по ключу @c key.
   * @details Синтаксис конфигурационного таков, что если указано несколько
   * ключей с разными значениями, то этот ключ имеет все указанные значения.
   * Например, такое происходит с @c TRUSTED_USER.
   *
   * @param[in] key Ключ.
   * @return Список (вектор) значений по ключам.
   */
  std::vector<std::string> getValues_(const std::string key) const;

  /**
   * @brief Метод, читающий содержимое конфигурационного файла.
   *
   * @param[in] enableLogging Флаг, запрещающий или разрешающий логирование в
   * консоль.
   * @return Содержимое конфигурационного файла.
   */
  std::string readConfig_(bool enableLogging = true);

  /**
   * @brief Метод, обновляющий значения конфигурационного файла.
   */
  void updateValues_();

public:
  /**
   * @brief Поле, содержащее текущие пропарсенные значения в конфигурационном
   * файле.
   */
  ConfigValues currentValues;

  /**
   * @brief Метод, определяющий действие при изменении конфигурационного файла.
   *
   * @note При вызове этого метода сразу выполняется action. Это нужно для
   * мгновенной инициализации значений в конфигурационном файле.
   * @note Интервал между обновлением файла конфигурации - 5 секунд.
   *
   * @param[in] action Анонимная функция, которая будет выполнена.
   */
  void onUpdate(std::function<void()> action);

  /**
   * @brief Конструктор, инициализирующий этот класс конфигурационном файлом по
   * пути @c configPath.
   *
   * @param[in] configPath Относительный путь к конфигурационному файлу.
   */
  ConfigManager(const std::string configPath);
};
